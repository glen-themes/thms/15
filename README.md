![Screenshot preview of the theme "Monster" by glenthemes](https://64.media.tumblr.com/56504d9a0eb6c720106f829a6797b103/5838e74db78e3f19-53/s1280x1920/c9de09b524af158ee7b32547356509fae648f77e.png)

**Theme no.:** 15  
**Theme name:** Monster  
**Theme type:** Free / Tumblr use  
**Description:** From ‘<small>HOLY</small>’ to ‘<small>MONSTER</small>’ – made for a PVRIS song in the past, this theme was reworked with the release of PVRIS’s new single ️‍🔥.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-12-21](https://64.media.tumblr.com/8adead1f89bea552a9c36287e91d7379/tumblr_nzpf5u6K581ubolzro2_r1_1280.png)  
**Rework date:** 2021-08-21

**Post:** [glenthemes.tumblr.com/post/660136693011218432](https://glenthemes.tumblr.com/post/660136693011218432)  
**Preview:** [glenthpvs.tumblr.com/monster](https://glenthpvs.tumblr.com/monster)  
**Download:** [glen-themes.gitlab.io/thms/15/monster](https://glen-themes.gitlab.io/thms/15/monster)  
**Credits:** [glencredits.tumblr.com/monster](https://glencredits.tumblr.com/monster)
